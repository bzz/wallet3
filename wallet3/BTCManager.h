#import <Foundation/Foundation.h>

@interface BTCManager : NSObject {}



@property (nonatomic) NSMutableString *secretKey;
@property (nonatomic) NSMutableString *secretKeyForChangeAddress;
@property (nonatomic) NSMutableString *recipientAddress;
@property (nonatomic) uint64_t amountToSend;
@property (nonatomic) uint64_t minersFee;

//TODO: - derive
@property (nonatomic) NSMutableString *secretKeyDerivedAddress;




@property (nonatomic) uint64_t balance;
@property (nonatomic, strong) NSArray *utxoDics;



@property (nonatomic) dispatch_queue_t Q;


- (NSString *)addressFromSecretKey:(NSString *)secretKey;
- (BOOL)isValidAddress:(NSString *)string;


- (NSString *)createTransaction;


- (void)testtest;

@end
