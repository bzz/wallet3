//
//  ecc_s.h
//  wallet2
//
//  Created by Mikhail Baynov on 21/02/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

#ifndef __BTC_ECC_S_H
#define __BTC_ECC_S_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>




#include "btc_ecc.h"


typedef struct {
    uint32_t n[8];
} secp256k1_fe_storage;

typedef struct {
    secp256k1_fe_storage x;
    secp256k1_fe_storage y;
} secp256k1_ge_storage;



typedef struct {
    /* For accelerating the computation of a*P + b*G: */
    secp256k1_ge_storage (*pre_g)[];    /* odd multiples of the generator */
} secp256k1_ecmult_context;

typedef struct {
    uint32_t d[8];
} secp256k1_scalar;
/** A group element of the secp256k1 curve, in jacobian coordinates. */

typedef struct {
    uint32_t n[10];
} secp256k1_fe;

typedef struct {
    secp256k1_fe x; /* actual X: x/z^2 */
    secp256k1_fe y; /* actual Y: y/z^3 */
    secp256k1_fe z;
    int infinity; /* whether this represents the point at infinity */
} secp256k1_gej;



typedef struct {
    /* For accelerating the computation of a*G:
     * To harden against timing attacks, use the following mechanism:
     * * Break up the multiplicand into groups of 4 bits, called n_0, n_1, n_2, ..., n_63.
     * * Compute sum(n_i * 16^i * G + U_i, i=0..63), where:
     *   * U_i = U * 2^i (for i=0..62)
     *   * U_i = U * (1-2^63) (for i=63)
     *   where U is a point with no known corresponding scalar. Note that sum(U_i, i=0..63) = 0.
     * For each i, and each of the 16 possible values of n_i, (n_i * 16^i * G + U_i) is
     * precomputed (call it prec(i, n_i)). The formula now becomes sum(prec(i, n_i), i=0..63).
     * None of the resulting prec group elements have a known scalar, and neither do any of
     * the intermediate sums while computing a*G.
     */
    secp256k1_ge_storage (*prec)[64][16]; /* prec[j][i] = 16^j * i * G + U_i */
    secp256k1_scalar blind;
    secp256k1_gej initial;
} secp256k1_ecmult_gen_context;


typedef struct {
    void (*fn)(const char *text, void* data);
    const void* data;
} secp256k1_callback;



struct secp256k1_context_struct {
    secp256k1_ecmult_context ecmult_ctx;
    secp256k1_ecmult_gen_context ecmult_gen_ctx;
    secp256k1_callback illegal_callback;
    secp256k1_callback error_callback;
};
typedef struct secp256k1_context_struct secp256k1_context;




typedef struct {
    unsigned char data[64];
} secp256k1_ecdsa_signature;


typedef int (*secp256k1_nonce_function)(
unsigned char *nonce32,
const unsigned char *msg32,
const unsigned char *key32,
const unsigned char *algo16,
void *data,
unsigned int attempt
);




#define SECP256K1_INLINE
/** All flags' lower 8 bits indicate what they're for. Do not use directly. */
#define SECP256K1_FLAGS_TYPE_MASK ((1 << 8) - 1)
#define SECP256K1_FLAGS_TYPE_CONTEXT (1 << 0)
#define SECP256K1_FLAGS_TYPE_COMPRESSION (1 << 1)
/** The higher bits contain the actual data. Do not use directly. */
#define SECP256K1_FLAGS_BIT_CONTEXT_VERIFY (1 << 8)
#define SECP256K1_FLAGS_BIT_CONTEXT_SIGN (1 << 9)
#define SECP256K1_FLAGS_BIT_COMPRESSION (1 << 8)

/** Flags to pass to secp256k1_context_create. */
#define SECP256K1_CONTEXT_VERIFY (SECP256K1_FLAGS_TYPE_CONTEXT | SECP256K1_FLAGS_BIT_CONTEXT_VERIFY)
#define SECP256K1_CONTEXT_SIGN (SECP256K1_FLAGS_TYPE_CONTEXT | SECP256K1_FLAGS_BIT_CONTEXT_SIGN)
#define SECP256K1_CONTEXT_NONE (SECP256K1_FLAGS_TYPE_CONTEXT)

#define WINDOW_G 16
/** The number of entries a table with precomputed multiples needs to have. */
#define ECMULT_TABLE_SIZE(w) (1 << ((w)-2))



//secp256k1_context* bzz_secp256k1_context_create(unsigned int flags);
//extern const secp256k1_nonce_function bzz_secp256k1_nonce_function_rfc6979;


int bzz_ecdsa_sign   (  secp256k1_ecdsa_signature *sig,
                        const unsigned char *msg32,
                        const unsigned char *seckey,
                        const void *ndata);


int bzz_ecdsa_signature_serialize_der(      unsigned char *output,
                                            size_t *outputlen,
                                            const secp256k1_ecdsa_signature* sig
                                            );




#endif /* ecc_s_h */
