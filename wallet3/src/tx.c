#include "tx.h"



size_t bbp_tx_size(const bbp_tx_t *tx, bbp_sighash_t flag) {
    size_t size = 0;
    int i;

    /* version */
    size += sizeof(uint32_t);

    /* inputs count */
    size += bbp_varint_size(tx->inputs_len);

    /* inputs */
    for (i = 0; i < tx->inputs_len; ++i) {
        bbp_txin_t *txin = &tx->inputs[i];

        /* outpoint */
        size += sizeof(bbp_outpoint_t);

        /* script */
        size += bbp_varint_size(txin->script_len);
        size += txin->script_len;

        /* sequence */
        size += sizeof(uint32_t);
    }

    /* outputs count */
    size += bbp_varint_size(tx->outputs_len);

    /* outputs */
    for (i = 0; i < tx->outputs_len; ++i) {
        bbp_txout_t *txout = &tx->outputs[i];

        /* value */
        size += sizeof(uint64_t);

        /* script */
        size += bbp_varint_size(txout->script_len);
        size += txout->script_len;
    }

    /* locktime */
    size += sizeof(uint32_t);

    if (flag) {

        /* sighash */
        size += sizeof(uint32_t);
    }

    return size;
}

void bbp_tx_serialize(const bbp_tx_t *tx, uint8_t *raw, bbp_sighash_t flag) {
    uint8_t *ptr;
    size_t varlen;
    int i;

    ptr = raw;

    /* version */
    *(uint32_t *)ptr = tx->version;
    ptr += sizeof(uint32_t);

    /* inputs count */
    bbp_varint_set(ptr, tx->inputs_len, &varlen);
    ptr += varlen;

    /* inputs */
    for (i = 0; i < tx->inputs_len; ++i) {
        bbp_txin_t *txin = &tx->inputs[i];

        /* outpoint */
        memcpy(ptr, txin->outpoint.txid, 32);
        ptr += 32;
        *(uint32_t *)ptr = txin->outpoint.index;
        ptr += sizeof(uint32_t);

        /* script */
        bbp_varint_set(ptr, txin->script_len, &varlen);
        ptr += varlen;
        memcpy(ptr, txin->script, txin->script_len);
        ptr += txin->script_len;

        /* sequence */
        *(uint32_t *)ptr = txin->sequence;
        ptr += sizeof(uint32_t);
    }

    /* outputs count */
    bbp_varint_set(ptr, tx->outputs_len, &varlen);
    ptr += varlen;

    /* outputs */
    for (i = 0; i < tx->outputs_len; ++i) {
        bbp_txout_t *txout = &tx->outputs[i];

        /* value */
        *(uint64_t *)ptr = txout->value;
        ptr += sizeof(uint64_t);

        /* script */
        bbp_varint_set(ptr, txout->script_len, &varlen);
        ptr += varlen;
        memcpy(ptr, txout->script, txout->script_len);
        ptr += txout->script_len;
    }

    /* locktime */
    *(uint32_t *)ptr = tx->locktime;
    ptr += sizeof(uint32_t);

    if (flag) {

        /* sighash */
        *(uint32_t *)ptr = flag;
    }
}
