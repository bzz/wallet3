#ifndef __VARINT_H
#define __VARINT_H

#include <string.h>


typedef enum {
    BBP_VARINT16 = 0xfd,
    BBP_VARINT32 = 0xfe,
    BBP_VARINT64 = 0xff
} bbp_varint_t;

uint64_t bbp_varint_get(uint8_t *bytes, size_t *len);

void bbp_varint_set(uint8_t *bytes, uint64_t n, size_t *len);

size_t bbp_varint_size(uint64_t n);


#endif
