#ifndef __BTC_BASE58_HEX_H
#define __BTC_BASE58_HEX_H
#ifdef __cplusplus
extern "C" {
#endif

#include <string.h>



char btc_hex_char2byte_char(const char in); //tmp


/******************* HEX **************************************/
/*  HEX2BIN                                                   */
/*  out_len   binary bytes count (IF NOT NEEDED, PASS NULL)   */
/*  out_len == strlen(in) / 2                                 */
void btc_hex2bin(const char *in, uint8_t** out, size_t* out_len);
uint8_t*       btc_bin_from_hex(const char* in, size_t* out_len);  //IF NOT NEEDED, PASS NULL

void btc_bin2hex(const uint8_t* data, const size_t data_len, char** out);
char* btc_hex_from_bin(const uint8_t* data, const size_t data_len);

/******************* BASE58 ***********************************/
void btc_bin2base58(const uint8_t* data, const size_t data_len, char* out);
int btc_base58tobin(const char *in, uint8_t* out, size_t *out_len);


void print_bytes(const uint8_t *bytes, size_t len);

   
#ifdef __cplusplus
}
#endif
#endif
