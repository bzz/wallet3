#ifndef btc_hash_h
#define btc_hash_h

#include <stdio.h>


#define SHA256_DIGEST_LENGTH        32



void sha256(const uint8_t *message, const size_t message_len, uint8_t digest[32]);
void ripemd160(const uint8_t *message, const uint32_t message_len, uint8_t digest[20]);

///double sha256
void hash256(const uint8_t *message, const size_t message_len, uint8_t digest[32]);

//ripemd160 of sha256
void hash160(uint8_t *message, const uint32_t message_len, uint8_t digest[20]);




#endif /* btc_hash_h */
