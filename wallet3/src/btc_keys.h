#ifndef __BTC_KEYS_H
#define __BTC_KEYS_H
//#ifdef __cplusplus
//extern "C" {
//#endif
#include <string.h>


#define BTC_ADDRESS_STRING_MAX_LENGTH           36
#define BTC_WIF_SECRET_KEY_STRING_MAX_LENGTH   100


typedef struct {
   uint8_t bytes[20];
} btc_hash160_t;



char* btc_wif_from_seckey(const uint8_t seckey[32]);
int btc_wif2seckey(const char* wif, uint8_t* seckey[32]);
int btc_wif_invalid(const char* wif);

char* btc_address_from_pubkey65(const uint8_t pubkey[65]);
int btc_address_invalid(const char* address);



//const char* btc_ophash160_hex_from_base58check(const char *str);

btc_hash160_t btc_hash160_from_addr(const char *address);





//#ifdef __cplusplus
//}
//#endif
#endif
