#include "btc_base58_hex.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>




/*                    */
/*  HELPER FUNCTIONS  */
/*                    */
char btc_hex_char2byte_char(const char in) {
   if ((in >= '0') && (in <= '9')) {
      return in - '0';
   }
   if ((in >= 'a') && (in <= 'f')) {
      return in - 'a' + 10;
   }
   if ((in >= 'A') && (in <= 'F')) {
      return in - 'A' + 10;
   }
   return 0;
}





void btc_hex2bin(const char *in, uint8_t** out, size_t* out_len)
{
   *out = malloc(strlen(in) / 2);
   
   for (size_t i = 0; i < strlen(in) / 2; ++i) {
      const char hi = btc_hex_char2byte_char(in[i * 2]);
      const char lo = btc_hex_char2byte_char(in[i * 2 + 1]);
      (*out)[i] = hi * 16 + lo;
   }
   if (out_len) {
      *out_len = strlen(in) / 2;
   }
}

uint8_t *btc_bin_from_hex(const char *in, size_t *out_len)
{
   uint8_t *out = malloc(strlen(in) / 2);
   
   for (size_t i = 0; i < strlen(in) / 2; ++i) {
      const char hi = btc_hex_char2byte_char(in[i * 2]);
      const char lo = btc_hex_char2byte_char(in[i * 2 + 1]);
      out[i] = hi * 16 + lo;
   }
   if (out_len) {
      *out_len = strlen(in) / 2;
   }   
   return out;
}


void btc_bin2hex(const uint8_t *data, const size_t data_len, char** out)
{
   char hex_buffer[data_len * 2 + 1];
   if (data) {
      for (size_t i = 0; i < data_len; i++)  {
         sprintf(&hex_buffer[i*2], "%02x", data[i]);
      }
   } else printf("btc_bin2hex - DATA EMPTY");
   *out = hex_buffer;
}


char* btc_hex_from_bin(const uint8_t *data, const size_t data_len)
{
   char *out = malloc(data_len * 2 + 1);
   if (data) {
      for (size_t i = 0; i < data_len; i++)  {
         sprintf(&out[i*2], "%02x", data[i]);
      }
   } else printf("btc_bin2hex - DATA EMPTY");
//   btc_bin2hex(data, data_len, &out);
   return out;
}



/* BASE58 ****************************************************************/

void btc_bin2base58(const uint8_t* data, const size_t data_len, char* out)
{
   const char b58digits_ordered[] = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
   int carry;
   ssize_t i, j, high, zcount = 0;
   size_t size;
   
   while (zcount < data_len && !data[zcount]) ++zcount;
   
   size = (data_len - zcount) * 138 / 100 + 1;
   uint8_t buf[size];
   memset(buf, 0, size);
   
   for (i = zcount, high = size - 1; i < data_len; ++i, high = j) {
      for (carry = data[i], j = size - 1; (j > high) || carry; --j) {
         carry += 256 * buf[j];
         buf[j] = carry % 58;
         carry /= 58;
      }
   }
   for (j = 0; j < size && !buf[j]; ++j);
   
   if (zcount)
      memset(out, '1', zcount);
   for (i = zcount; j < size; ++i, ++j)
      out[i] = b58digits_ordered[buf[j]];
   out[i] = '\0';
}




int btc_base58tobin(const char *str, uint8_t* out, size_t *out_len)
{
   const size_t len = strlen(str);
   
   size_t i = 0;
   size_t z = 0;
   
   while (z < len && !str[z]) ++z; // count leading zeroes
   
   
   uint8_t buf[(len - z)*733/1000 + 1]; // log(58)/log(256), rounded up
   
   memset(buf, 0, sizeof(buf));
   
   for (i = z; i < len; ++i) {
      uint32_t carry = str[i];
      
      switch (carry) {
         case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
            carry -= '1';
            break;
            
         case 'A': case 'B': case 'C': case 'D': case 'E': case 'F': case 'G': case 'H':
            carry += 9 - 'A';
            break;
            
         case 'J': case 'K': case 'L': case 'M': case 'N':
            carry += 17 - 'J';
            break;
            
         case 'P': case 'Q': case 'R': case 'S': case 'T': case 'U': case 'V': case 'W': case 'X': case 'Y':
         case 'Z':
            carry += 22 - 'P';
            break;
            
         case 'a': case 'b': case 'c': case 'd': case 'e': case 'f': case 'g': case 'h': case 'i': case 'j':
         case 'k':
            carry += 33 - 'a';
            break;
            
         case 'm': case 'n': case 'o': case 'p': case 'q': case 'r': case 's': case 't': case 'u': case 'v':
         case 'w': case 'x': case 'y': case 'z':
            carry += 44 - 'm';
            break;
            
         default:
            carry = UINT32_MAX;
      }
      
      if (carry >= 58)
         return 1; // invalid base58 digit
      
      for (size_t j = sizeof(buf); j > 0; --j) {
         carry += (uint32_t)buf[j - 1]*58;
         buf[j - 1] = carry & 0xff;
         carry >>= 8;
      }
      
      memset(&carry, 0, sizeof(carry));
   }
   
   i = 0;
   while (i < sizeof(buf) && buf[i] == 0) i++; // skip leading zeroes
   
   *out_len = sizeof(buf) - i;
   memcpy(out, &buf[i], *out_len);
   return 0;
}


void print_bytes(const uint8_t *bytes, size_t len) {
   printf("\nprint_bytes: %s\n", btc_hex_from_bin(bytes, len));
}


