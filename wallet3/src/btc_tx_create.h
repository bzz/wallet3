#ifndef __BTC_TX_CREATE_H
#define __BTC_TX_CREATE_H

#include "btc_keys.h"


typedef struct {
   uint64_t balance;
   int32_t index;
   uint8_t* id;
   size_t id_len;
} btc_txinput;

typedef struct {
   uint64_t amount;
   btc_hash160_t hash160;
} btc_txoutput;



int btc_tx_create(uint8_t seckey[32],
                  uint8_t change_addr_seckey[32],
                  uint64_t fee,
                  btc_txinput  *inputs,
                  size_t inputs_count,
                  btc_txoutput *outputs,
                  size_t outputs_count,
                  
                  uint8_t** out, size_t* out_len);











#endif /* create_tx_h */
