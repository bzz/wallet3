#ifndef __TX_H
#define __TX_H

#include <stdint.h>
#include "varint.h"



typedef struct {
    uint64_t value;
    uint64_t script_len;
    uint8_t* script;
} bbp_txout_t;

typedef struct {
    uint8_t txid[32];
    uint32_t index;
} bbp_outpoint_t;

typedef struct {
    bbp_outpoint_t outpoint;
    uint64_t script_len;
    uint8_t* script;
    uint32_t sequence;
} bbp_txin_t;

typedef struct {
    uint32_t version;
    uint64_t inputs_len;
    bbp_txin_t* inputs;
    uint64_t outputs_len;
    bbp_txout_t* outputs;
    uint32_t locktime;
} bbp_tx_t;

typedef enum {
    BBP_SIGHASH_ALL = 0x01
} bbp_sighash_t;




size_t bbp_tx_size(const bbp_tx_t *tx, bbp_sighash_t flag);


void bbp_tx_serialize(const bbp_tx_t *tx, uint8_t *raw, bbp_sighash_t flag);


#endif
