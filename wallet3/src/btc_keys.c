#include "btc_keys.h"

#include <stdlib.h>
#include "btc_base58_hex.h"
#include "btc_hash.h"



#define BITCOIN_TESTNET 1



char* btc_wif_from_seckey(const uint8_t seckey[32]) {
   uint8_t data[37];
   memcpy(&data[1], seckey, 32);
#if BITCOIN_TESTNET
   data[0] = 0xef;
#else
   data[0] = 0x80;
#endif
   uint8_t hash[32];
   sha256(data, 33, hash);
   sha256(hash, 32, hash);
   memcpy(&data[33], hash, 4);
   char *out = calloc(1, BTC_WIF_SECRET_KEY_STRING_MAX_LENGTH);
   btc_bin2base58(data, 37, out);
   return out;
}



int btc_wif2seckey(const char* wif, uint8_t* seckey[32])
{
   size_t data_len = 37;
   uint8_t data[data_len];
   btc_base58tobin(wif, &data, &data_len);
   if (data[0] != 0x80 && data[0] != 0xef) {
      return 2;     //Invalid WIF key
   }
   uint8_t *checksum = &data[data_len-4];
   if (data_len == 37) {
      data_len -= 4;
   } else if (data_len == 38) {  //compressed public key, extra byte 0x01
      data_len -= 5;
   }
   uint8_t hash[32];
   sha256(data, 33, hash);
   sha256(hash, 32, hash);
   if (memcmp(hash, checksum, 4)) {
      return 1;    //Failed WIF key checksum
   }
   if (seckey) {
      memcpy(*seckey, &data[1], 32);
   }
   return 0;
}
int btc_wif_invalid(const char* wif)
{
   return btc_wif2seckey(wif, NULL);
}



char* btc_address_from_pubkey65(const uint8_t pubkey[65])
{
   uint8_t digest[20];
   hash160(pubkey, 65, digest);
   print_bytes(digest, 20);
   
   uint8_t result[25];
   uint8_t hash[21];
#if BITCOIN_TESTNET
   result[0] = 111;
#else
   result[0] = 0;
#endif
   memcpy(&result[1], digest, 20);
   sha256(result, 21, hash);
   sha256(hash, 32, hash);
   memcpy(&result[21], hash, 4);
   
   char *out = calloc(100, 1);
   btc_bin2base58(result, 25, out);
   return out;
}
int btc_address_invalid(const char* address)
{
   size_t data_len = 26;
   uint8_t *data = calloc(1, data_len);
   btc_base58tobin(address, &data[1], &data_len);
   if (data_len == 24) {
      ++data_len;
   } else {
      data = &data[1];   //leak 1 byte
   }
   if (data_len != 25)
      return 2;
   uint8_t hash[32];
   hash256(data, 21, hash);
   
   if (memcmp(&data[data_len - 4], hash, 4)) {
      return 1;    //Failed checksum
   }
   return 0;
}




//const char* btc_ophash160_hex_from_base58check(const char *str) {
//#warning ADD BASE58CHECK VALIDITY CHECK!
//   if (strlen(str) < 4) return NULL;
//   
//   uint8_t data[1000];
//   size_t data_len;
//   btc_base58tobin(str, &data, &data_len);
//   
//   uint8_t out[data_len - 5];
//   memcpy(out, &data[1], data_len - 5);
//   return btc_hex_from_bin(out, data_len - 5);
//}


btc_hash160_t btc_hash160_from_addr(const char *address) {
#warning ADD BASE58CHECK VALIDITY CHECK!
   btc_hash160_t out = {0};
   if (strlen(address) < 4) return out;
   size_t data_len = 25;
   uint8_t data[data_len];
   btc_base58tobin(address, data, &data_len);
   if (data_len != 25) return out;
   memcpy(out.bytes, &data[1], 20);
   return out;
}

