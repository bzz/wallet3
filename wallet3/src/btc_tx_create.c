#include "btc_tx_create.h"

#include "btc_headers.h"

#include "tx.h"







char* der_signature_hex_for_message_and_key(const uint8_t* message, const size_t message_len, uint8_t key[32])
{
   uint8_t *hash[32];
   hash256(message, message_len, hash);
   size_t len = 100;
   uint8_t bytes[len];
   secp256k1_ecdsa_signature s;
   bzz_ecdsa_sign(&s, &hash, key, NULL);
   bzz_ecdsa_signature_serialize_der(bytes, &len, &s);
   return btc_hex_from_bin(bytes, len);
}





int btc_tx_create(uint8_t seckey[32],
                  uint8_t change_addr_seckey[32],
                  uint64_t fee,
                  btc_txinput  *inputs,
                  size_t inputs_count,
                  btc_txoutput *outputs,
                  size_t outputs_count,
                  
                  uint8_t** out, size_t* out_len)
{
   
   
   bbp_txout_t outs[outputs_count+1];
   bbp_txout_t prev_outs[inputs_count];
   bbp_txin_t ins_sign[inputs_count];
   bbp_txin_t ins[inputs_count];
   bbp_tx_t tx;
   uint8_t *msg;
   size_t msg_len;
   
   printf("\nbtc_tx_create inputs:\n");
   for (size_t i=0; i<inputs_count; ++i) {
      printf("%i/// %llu\n", inputs[i].index, inputs[i].balance);
   }
   
   
   uint8_t pubkey65[65];
   uECC_get_public_key65(seckey, pubkey65);
   uint8_t change_addr_pubkey65[65];
   uECC_get_public_key65(change_addr_seckey, change_addr_pubkey65);
   
   
   uint8_t change_addr_hash160[20];
   hash160(change_addr_pubkey65, 65, change_addr_hash160);
   const char *change_addr = btc_hex_from_bin(change_addr_hash160, 20);
   
   
   
   char *script = malloc(1000);
   
   
   uint64_t change = 0;
   for (size_t i=0; i<inputs_count; ++i) {
      sprintf(script, "76a914%s88ac", change_addr);
      btc_hex2bin(script, &prev_outs[i].script, (size_t *)&prev_outs[i].script_len);
      prev_outs[i].value = inputs[i].balance;
      change += inputs[i].balance;
   }
   for (size_t i=0; i<outputs_count; ++i) {
      sprintf(script, "76a914%s88ac", btc_hex_from_bin(outputs[i].hash160.bytes, 20));
      btc_hex2bin(script, &outs[i+1].script, (size_t *)&outs[i+1].script_len);
      outs[i+1].value = outputs[i].amount;
      change -= outputs[i].amount;
   }
   change -= fee;
   
   sprintf(script, "76a914%s88ac", change_addr);
   btc_hex2bin(script, &outs[0].script, (size_t *)&outs[0].script_len);
   outs[0].value = change;
   
   
   
   
   /* message */
   tx.version = 1;
   tx.outputs_len = outputs_count+1; //+1 for change
   tx.inputs_len = inputs_count;
   tx.locktime = 0;
   tx.outputs = outs;
   
   
   
   
   
   
   for (size_t i=0; i<inputs_count; ++i) {
      for (size_t n=0; n<32; ++n) {
         ins_sign[i].outpoint.txid[n] = inputs[i].id[31-n];   //reverse copy
         ins[i].outpoint.txid[n] = inputs[i].id[31-n];
      }
      ins_sign[i].outpoint.index = inputs[i].index;
      ins_sign[i].sequence = 0xffffffff;
      ins[i].outpoint.index = inputs[i].index;
      ins[i].sequence = 0xffffffff;
   }
   
   
   
   
   for (size_t i=0; i<inputs_count; ++i) {
      for (size_t n=0; n<inputs_count; ++n) {
         ins_sign[n].script_len = 0;
         ins_sign[n].script = "";
      }
      ins_sign[i].script_len = prev_outs[i].script_len;
      ins_sign[i].script = malloc(prev_outs[i].script_len);
      memcpy(ins_sign[i].script, prev_outs[i].script, prev_outs[i].script_len);
      
      tx.inputs = ins_sign;
      msg_len = bbp_tx_size(&tx, BBP_SIGHASH_ALL);
      msg = malloc(msg_len);
      bbp_tx_serialize(&tx, msg, BBP_SIGHASH_ALL);
      
      
      char* der_signature_hex = der_signature_hex_for_message_and_key(msg, msg_len, seckey);
      
      sprintf(script, "%02lx%s%02x%02lx%s", strlen(der_signature_hex)/2 + 1, der_signature_hex, BBP_SIGHASH_ALL, (size_t)65, btc_hex_from_bin(pubkey65, 65));
      
      btc_hex2bin(script, &ins[i].script, (size_t *)&ins[i].script_len);
   }
   
   
   
   
   
   /* packing */
   tx.outputs = outs;
   tx.inputs = ins;
   
   
   uint8_t *rawtx;
   size_t rawtx_len;
   rawtx_len = bbp_tx_size(&tx, 0);
   rawtx = malloc(rawtx_len);
   bbp_tx_serialize(&tx, rawtx, 0);
   
   ////   /* txid (print big-endian) */
   ////   uint8_t txid[32];
   ////   bbp_hash256(txid, rawtx, rawtx_len);
   ////   bbp_reverse(txid, 32);
   
   memcpy(out, rawtx, rawtx_len);
   *out_len = rawtx_len;
   
   return 0;
}



