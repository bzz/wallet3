#import "BTCManager.h"

#import "btc_headers.h"

@implementation BTCManager

//TBD:


- (NSString *)addressFromSecretKey:(NSString *)secretKey {
   uint8_t *seckey = btc_bin_from_hex(secretKey.UTF8String, NULL);
   uint8_t pubkey[65];
   uECC_get_public_key65((uint8_t*) seckey, pubkey);
   return [NSString stringWithUTF8String:btc_address_from_pubkey65(pubkey)];
}

 

- (BOOL)isValidAddress:(NSString *)string {
   return btc_address_invalid(string.UTF8String);
}



- (NSString *)createTransaction {
   btc_txinput *inputs = calloc(sizeof(btc_txinput), self.utxoDics.count);
   
   NSMutableDictionary *dic;
   for (int32_t i = 0; i < self.utxoDics.count; ++i) {
      dic = self.utxoDics[i];
      if (dic) {
         NSNumber *value = dic[@"value"];
         inputs[i].balance = value.intValue;
         
         NSNumber *tx_output_n = dic[@"tx_output_n"];
         
         inputs[i].index = tx_output_n.intValue;

         NSString *txHash = dic[@"tx_hash"];
         btc_hex2bin(txHash.UTF8String, &inputs[i].id, &inputs[i].id_len);
         NSLog(@"input N: %i  BALANCE: %llu ", inputs[i].index, inputs[i].balance);
         
      }
   }
   
   
   btc_txoutput outputs[] = {
//      {50000, btc_hash160_from_addr("msaMCB38e227VRKbk3FEXocCmJLcN4tJJw")},
//      {50000, btc_hash160_from_addr("msaMCB38e227VRKbk3FEXocCmJLcN4tJJw")},
      {self.amountToSend, btc_hash160_from_addr(self.recipientAddress.UTF8String)},
//      {15000, btc_hash160_from_addr("mpL9RRBhJZp5T3rprkun5mefk61uX6Cy4p")},
   };


   
   uint8_t signed_msg[10000];
   size_t signed_msg_len;
   btc_tx_create(btc_bin_from_hex(self.secretKey.UTF8String, NULL),
                 btc_bin_from_hex(self.secretKeyForChangeAddress.UTF8String, NULL),
                 self.minersFee, inputs, self.utxoDics.count, outputs, 1, &signed_msg, &signed_msg_len);
   
   
   printf("\nSIGNED MESSAGE:\n%s", btc_hex_from_bin(signed_msg, signed_msg_len));
   
   
   
   
//   if (is_vdata_equal(signed_message, vdata_new_from_hex("0100000001f0ad277ed85fabe6ed51c84dc29f4d089b8ff51a005265149ab6372b278a1ffa000000008a47304402200e838d946017214e7eba74c9aacf3d7967ef23a1caeb946198ee0ce1c7b6e3d7022075bff61ca9ae89f19a817021ccd742b602b59c9839200b68715528078e3d258c01410461d6e29695d5ac7575cdc876368fee939b4f003798599d76208f57f5ebc5cf2f2dab9c59a2179c98159b991a412fef237535ac167b88c4c6bdaff791f792ed05ffffffff0298809501000000001976a9148445df4193c05dd9be6c005f9c6d878ad1af0beb88aca8610000000000001976a914818ceac4fe31bb96a8ae556647668091807a652b88ac00000000"))) {
//      printf("SUCCESS\n\n");
//   } else printf("FAIL!!!!!!!\n\n");
   
   return @"";
}



- (void)testtest {

   
   size_t in_len;
   uint8_t *in_bytes;
   btc_hex2bin("00010966776006953D5567439E5E39F86A0D273BEED61967F6", &in_bytes, &in_len);
   
   char *out  = calloc(1, BTC_ADDRESS_STRING_MAX_LENGTH);
   btc_bin2base58(in_bytes, in_len, out);
   printf("QQQQQQQ %s\n", out);
   
  
   
   int rv;
   const char *addr = "16UwLL9Risc3QfPqBUvKofHmBQ7wMtjvM";
   const size_t addrlen = 0;// strlen(addr);
   size_t bin_len;
//   char bufx[26] = {'\xff'};
   uint8_t bin[25];
   bin_len = 25;
//   btc_base58tobin(addr, buf, &actuallen);
//   if (bufx[0] != '\xff')
//      exit(2);
   
   btc_base58tobin(addr, bin, &bin_len);
   printf("btc_base58tobin>   %s\n", btc_hex_from_bin(bin, bin_len));
   
   
   char *h = calloc(1, 1000);
   btc_bin2hex(bin, bin_len, &h);
   printf("btc_bin2hex>       %s\n", h);
   printf("btc_hex_from_bin> %s\n", btc_hex_from_bin(bin, bin_len));
   
   
   
   char *y = calloc(1, 1000);
   
   btc_bin2base58(bin, bin_len, y);
   printf("btc_bin2base58> %s\n\n\n\n", y);
   
   char *a = btc_wif_from_seckey(btc_bin_from_hex("0C28FCA386C7A227600B2FE50B7CAE11EC86D3BF1FBE471BE89827E19D72AA1D", NULL));
   printf("btc_wif_from_seckey>  %s\n", a);
   
   uint8_t *sec = calloc(1, 32);
   btc_wif2seckey(a, &sec);
   printf("btc_wif2seckey> return %i", btc_wif_invalid(a));
   print_bytes(sec, 32);
   
   
   char* pubk = "0450863AD64A87AE8A2FE83C1AF1A8403CB53F53E486D8511DAD8A04887E5B23522CD470243453A299FA9E77237716103ABC11A1DF38855ED6F2EE187E9C582BA6";
   uint8_t *ww = btc_bin_from_hex(pubk, 0);
   print_bytes(ww, 65);
   char *ad = btc_address_from_pubkey65(ww);
   printf("ADDR  %s\n", ad);
   
   printf("btc_address_invalid>%s return %i\n",ad, btc_address_invalid("16UwLL9Risc3QfPqBUvKofHmBQ7wMtjvM"));

   printf("btc_address_invalid>%s return %i\n","msaMCB38e227VRKbk3FEXocCmJLcN4tJJw", btc_address_invalid("msaMCB38e227VRKbk3FEXocCmJLcN4tJJw"));

   
   
}

@end
