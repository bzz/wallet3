import Foundation


public enum NotifierEvent : String {
   case gotAccountData = "gotAccountData"
   
}



public final class Notifier { 

   public static func post(event: NotifierEvent) {
      print("NOTIFIER: \(event.rawValue)")
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: event.rawValue), object:self)
   }   
   public static func post(event: NotifierEvent, userInfo: [NSObject : AnyObject]) {
      print("NOTIFIER: \(event.rawValue) \(userInfo.description)")
      NotificationCenter.default.post(name: NSNotification.Name(rawValue: event.rawValue), object:self, userInfo:userInfo)
   }
   
   
   public static func addObserver(observer: AnyObject, event : NotifierEvent) {
      NotificationCenter.default.addObserver(observer, selector: Selector(event.rawValue), name: NSNotification.Name(rawValue: event.rawValue), object :nil)
   }
   
   public static func removeObserver(observer: AnyObject, event : NotifierEvent) {
      NotificationCenter.default.removeObserver(observer, name: NSNotification.Name(rawValue: event.rawValue), object: nil)
   }

}
