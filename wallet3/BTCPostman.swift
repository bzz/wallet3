import Foundation


open class BTCPostman : NSObject {
   
   let session = URLSession.shared
   var datastring = String()
   var Q : DispatchQueue?
   
   

   var apiTestnetURL = "https://api.blockcypher.com/v1/btc/test3/addrs/"
   var apiURL =        "https://api.blockcypher.com/v1/btc/main/addrs/"
   
   
   
   
   override init() {
      super.init()
      Q = DispatchQueue(label: "postman.background.q", attributes: []);
   }
   
   func getData(_ address: String, testnet: Bool) {
      DispatchQueue.global().async {
         
         //TODO: Check address for validity
         
         var tmp : String
         if testnet {
            tmp = self.apiTestnetURL
         } else {
            tmp = self.apiURL
         }
         
         let url = URL(string: tmp + address + "?unspentOnly=true")
         let request = NSMutableURLRequest(url: url!)
         request.httpMethod = "GET"
         request.addValue("application/json", forHTTPHeaderField: "Content-Type")
         request.addValue("application/json", forHTTPHeaderField: "Accept")
         
         //?unspentOnly=true
         
         
         let task = self.session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            guard data != nil else {
               print("no data found: \(error)")
               return
            }
            do {
               self.datastring = NSString(data:data!, encoding:String.Encoding.utf8.rawValue) as! String
               print("GOT DATA FROM SERVER: \(self.datastring)")
               
               if let dic = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
//                  print(dic.description)
                  DispatchQueue.main.async {
                     
                     guard dic["final_balance"] != nil else { return }
                     let final_balance = dic["final_balance"]
//                     print("final_balance ", final_balance!)
                     appDelegate.btcmanager.balance = UInt64(final_balance as! Int)
                     Notifier.post(event: .gotAccountData)
                     

                     guard let UTXOs = dic["txrefs"] as? NSArray else { return }
                     guard UTXOs.count != 0 else { print("no transactions"); return }
                     
                     
                     appDelegate.btcmanager.utxoDics = UTXOs as! [Any]
                  }

                  
               } else {
                  let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                  print("Error could not parse JSON: \(jsonStr)")
               }
            } catch let error {
               print(error)
               let jsonStr = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
               print("Error could not parse JSON: '\(jsonStr)'")
            }
         }) 
         task.resume()
         
         
      }
   }

}
