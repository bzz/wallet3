//
//  ViewController.swift
//  wallet3
//
//  Created by Mikhail Baynov on 13/09/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

import UIKit

class SendVC: UIViewController {

   
   @IBOutlet weak var testnetSwitch: UISwitch!
   @IBOutlet weak var secretKeyField: UITextField!
   
   @IBOutlet weak var secretKeyForChangeAddressField: UITextField!
   
   @IBOutlet weak var recipientAddressField: UITextField!
   @IBOutlet weak var amountToSendField: UITextField!
   @IBOutlet weak var minersFeeField: UITextField!
   
   @IBOutlet weak var maxAmountLabel: UILabel!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      

      secretKeyField.text = appDelegate.btcmanager.secretKey as String?
      secretKeyForChangeAddressField.text = appDelegate.btcmanager.secretKeyForChangeAddress as String?
      recipientAddressField.text = appDelegate.btcmanager.recipientAddress as String?
      amountToSendField.text = String(appDelegate.btcmanager.amountToSend)
      minersFeeField.text = String(appDelegate.btcmanager.minersFee)
      
      
      //      btcpostman.getData("1F1tAaz5x1HUXrCNLbtMDqcw6o5GNn4xqX", testnet: false)
      appDelegate.btcpostman.getData(appDelegate.btcmanager.secretKeyDerivedAddress as String, testnet: true)
      
      
      secretKeyField.delegate = self
      secretKeyForChangeAddressField.delegate = self
      recipientAddressField.delegate = self
      amountToSendField.delegate = self
      minersFeeField.delegate = self
      
      
      Notifier.addObserver(observer: self, event: .gotAccountData)
   }
   
   deinit {
      Notifier.removeObserver(observer: self, event: .gotAccountData)
   }
   
   func gotAccountData() {
      self.maxAmountLabel.text = "max: " + String(appDelegate.btcmanager.balance)
   }

   


   @IBAction func testnetSwitchMoved(_ sender: UISwitch) {
      print(sender.isOn)
   }
   
   @IBAction func sendButtonPressed(_ sender: UIButton) {
      appDelegate.btcmanager.createTransaction()
      self.maxAmountLabel.text = "max: " + String(appDelegate.btcmanager.balance)
      
   }

}


extension SendVC: UITextFieldDelegate {
   

   func textFieldShouldReturn(_ textField: UITextField) -> Bool {
      switch textField.tag {
      case 101:
         appDelegate.btcmanager.secretKey = (textField.text! as NSString).mutableCopy() as! NSMutableString
         print("appDelegate.btcmanager.secretKey")
         print(appDelegate.btcmanager.secretKey)
         break
      case 102:
         appDelegate.btcmanager.secretKeyForChangeAddress = (textField.text! as NSString).mutableCopy() as! NSMutableString
         print("appDelegate.btcmanager.secretKeyForChangeAddress")
         print(appDelegate.btcmanager.secretKeyForChangeAddress)
         break
      case 103:
         appDelegate.btcmanager.recipientAddress = (textField.text! as NSString).mutableCopy() as! NSMutableString
         print("appDelegate.btcmanager.recipientAddress")
         print(appDelegate.btcmanager.recipientAddress)
         break
      case 104:
         if let a = UInt64(textField.text!) {
            appDelegate.btcmanager.amountToSend = a
            print("amountToSend")
            print(appDelegate.btcmanager.amountToSend)
         }
         break
      case 105:
         if let a = UInt64(textField.text!) {
            appDelegate.btcmanager.minersFee = a
            print("minersFee")
            print(appDelegate.btcmanager.minersFee)
         }
         break
      default: break
      }
      return true
   }
   
   func textFieldDidEndEditing(_ textField: UITextField) {
      print("textFieldDidEndEditing")
   }
   
   func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
      print("textFieldShouldEndEditing")
      return true
   }
 
   func textFieldDidBeginEditing(_ textField: UITextField) {
      print("textFieldDidBeginEditing")
   }
   
   
   
   
}

