//
//  AppDelegate.swift
//  wallet3
//
//  Created by Mikhail Baynov on 13/09/16.
//  Copyright © 2016 Mikhail Baynov. All rights reserved.
//

import UIKit

//Private key 1: 119f56f7b969f8118e7e68cdc825966814d4ef61c09b3b2823ae5b0a76359746
//Public key:  0461d6e29695d5ac7575cdc876368fee939b4f003798599d76208f57f5ebc5cf2f2dab9c59a2179c98159b991a412fef237535ac167b88c4c6bdaff791f792ed05
//OP_HASH160:  8445df4193c05dd9be6c005f9c6d878ad1af0beb
//addr:        msaMCB38e227VRKbk3FEXocCmJLcN4tJJw


//Private key 2: 16260783e40b16731673622ac8a5b045fc3ea4af70f727f3f9e92bdd3a1ddc42
//Public key:  0482006e9398a6986eda61fe91674c3a108c399475bf1e738f19dfc2db11db1d28130c6b3b28aef9a9c7e7143dac6cf12c09b8444db61679abb1d86f85c038a58c
//OP_HASH160:  818ceac4fe31bb96a8ae556647668091807a652b
//addr:        msKxGtZDoXeWjnxPNJbvZCBmLVv1fNqQMd


let appDelegate = UIApplication.shared.delegate as! AppDelegate


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
   var window: UIWindow?

   
   let btcmanager = BTCManager()
   let btcpostman = BTCPostman()

   var inputsArray = NSArray()
   
   
   func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
      
      btcmanager.secretKey =
      "119f56f7b969f8118e7e68cdc825966814d4ef61c09b3b2823ae5b0a76359746"
      btcmanager.secretKeyForChangeAddress =
      "119f56f7b969f8118e7e68cdc825966814d4ef61c09b3b2823ae5b0a76359746"
      
      btcmanager.recipientAddress = "msKxGtZDoXeWjnxPNJbvZCBmLVv1fNqQMd"
      btcmanager.amountToSend = 10000
      btcmanager.minersFee = 10000
      
      
      //tmp
      let s = btcmanager.address(fromSecretKey: btcmanager.secretKey as String!)
      btcmanager.secretKeyDerivedAddress = NSMutableString(string: s!)
//      btcmanager.secretKeyDerivedAddress = "msaMCB38e227VRKbk3FEXocCmJLcN4tJJw"
      
      
      btcmanager.testtest()
      return true
   }
   

}

